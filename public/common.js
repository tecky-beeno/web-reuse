async function get(url, cb) {
  let res = await fetch(url)
  let json = await res.json()
  cb(json)
}

function renderToList(list, template, array) {
  for (let order of array) {
    let node = template.cloneNode(true)
    for (let key in order) {
      let selector = '.' + key
      let element = node.querySelector(selector)
      if (!element) {
        selector = `[data-col="${key}"]`
        element = node.querySelector(selector)
      }
      if (element) {
        element.textContent = order[key]
      } else {
        console.debug('Warning: cannot find ' + selector)
      }
    }
    list.appendChild(node)
  }
}
