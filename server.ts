import express from 'express'
import { readFile, readFileSync } from 'fs'
import { print } from 'listening-on'
import { join } from 'path'

let app = express()

app.use(express.static('public'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

let skeleton = readFileSync(join('public', 'skeleton.html')).toString()
let page404 = readFileSync(join('public', '404.html'))
app.get('/page/:page', (req, res, next) => {
  let page = req.params.page
  let file = join('public', page + '.html')
  let title = page.slice(0, 1).toUpperCase() + page.slice(1)
  readFile(file, (err, content) => {
    if (err) {
      res.status(404)
      content = page404
    }
    let html = skeleton
      .replace('<title></title>', `<title>${title}</title>`)
      .replace(
        '<div class="page-title"></div>',
        `<div class="page-title">${title}</div>`,
      )
      .replace('<main></main>', `<main>${content}</main>`)
    res.end(html)
  })
})

app.get('/api/orders', (req, res) => {
  res.json({
    orders: [
      {
        order_id: 1,
        apply_date: '2023/12/23',
        service_type: 'Air Conditioning',
        event_date: '2023/12/25',
      },
      {
        order_id: 2,
        apply_date: '2023/12/24',
        district: 'KT',
        service_type: 'Air Hockey',
        event_date: '2023/12/26',
      },
    ],
  })
})

app.get('/api/requests', (req, res) => {
  res.json({
    requests: [
      {
        id: 1,
        title: 'Fix air conditioner',
        images: [
          'https://picsum.photos/seed/1/400/600',
          'https://picsum.photos/seed/2/800/600',
        ],
      },
      {
        id: 2,
        title: 'Fix fans',
        images: [
          'https://picsum.photos/seed/3/200/200',
          'https://picsum.photos/seed/4/800/200',
          'https://picsum.photos/seed/5/200/200',
        ],
      },
    ],
  })
})

let port = 8100
app.listen(port, () => {
  print(port)
})
